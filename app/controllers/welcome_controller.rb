require 'net/http'
require 'uri'
require 'json'

class WelcomeController < ApplicationController
	VK_APP_ID = 2889618
	VK_APP_SECRET = 'iXr8ZxBz9J9UJ3hxNE1s'

	FB_APP_ID = 185606301557957
	FB_APP_SECRET = 'e94d735549cb5a19a04590150c6eae92'
	#'http://vk.com/editapp?id=2889618&section=options'

	def index
	end

	def fbchannel
	end

	def fbtest
		@app_id = FB_APP_ID

		if params[:code]
			uri = URI.parse('https://graph.facebook.com/oauth/access_token')

			http = Net::HTTP.new(uri.host, uri.port)
			http.use_ssl = true

			response = http.post(uri.path, "client_id=#{FB_APP_ID}&code=#{params[:code]}&client_secret=#{FB_APP_SECRET}&redirect_uri=http://127.0.0.1:3000/fbtest").body
			
			@data = response.split('&').inject({}) {|h, i| p=i.split('=');h[p[0].to_sym] = p[1];h }

			session[:fb_access_token] = nil
			if !@data['error']
				@access_token = @data[:access_token]
				session[:fb_access_token] = @access_token
			end

			render :text => @data.to_s
		end
	end

	def facebook
	end

	def fbtest2
	end

	def vktest
		@app_id = APP_ID
	end

	def vkontakte
		uri = URI.parse('https://api.vk.com/oauth/token')

		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = true

		@data = {}
		@data = JSON.parse(http.post(uri.path, "client_id=#{APP_ID}&code=#{params[:code]}&client_secret=#{APP_SECRET}").body)

		session[:vk_access_token] = nil
		if !@data['error']
			@access_token = @data['access_token']
			session[:vk_access_token] = @access_token
		end

	end

	def vktest2
		access_token = session[:vk_access_token]
		session = VkApi::Session.new APP_ID, APP_SECRET
		@f = session.getUserInfoEx :uid => 7395427, :access_token => access_token
	end
	
end