class UsersController < ApplicationController
  load_and_authorize_resource
  #before_filter :authenticate_user!, :except => [:index]
  before_filter :current_page

  # Does @user = User.find(params[:id])
  #before_filter :load_user, :only => [:show, :edit, :create, :destroy]

  def current_page
    @users_cp_class = 'current'
  end

  # GET /users
  # GET /users.json
  def index
    @users = User.all
    authorize! :list, @users

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user ||= current_user
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    # authorize!
    
    # update roles, if can
   
    if can? :assign_roles, current_user
      checked_roles = params[:roles] || {}
      @user.role_id = []
      checked_roles.keys.each do |key|
        Role.first(conditions: {name: key }).users << @user
      end
    end

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    authorize! :destroy, @user
    
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  private
  # GET /users/new
  # GET /users/new.json
  def new
    #@user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  protected
    def load_user
      @user = User.find(params[:id])
    end

end
