class Users::RegistrationsController < Devise::RegistrationsController
	before_filter :current_item

	def current_item
		@users_cp_class = 'current'
	end

  #before_filter :check_permissions, :only => [:new, :create, :cancel]
  #skip_before_filter :require_no_authentication
 
  def check_permissions
    #authorize! :create, resource
  end

protected

  def after_sign_up_path_for(resource)
  	"/users/#{current_user.id.to_s}/edit"
  end

end