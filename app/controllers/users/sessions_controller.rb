class Users::SessionsController < Devise::SessionsController
	before_filter :current_item

	def current_item
		@users_cp_class = 'current'
	end

end