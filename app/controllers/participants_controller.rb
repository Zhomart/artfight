class ParticipantsController < ApplicationController
  #skip_authorize_resource :only => :create
  load_and_authorize_resource  :except => :like
  
  
  def like
    @participant = Participant.find(params[:id])

    redirect_to :back and return if !@participant.competition.running?

    token = "123123321"
    token = current_user._id.to_s if current_user

    key = 'liked_'+@participant.id.to_s+'_'+token
    
    if !cookies[key]
      @participant.like += 1
      @participant.save!
    end

    cookies[key] = { :value => true, :expires => Time.now + 60*60*24*365}

    redirect_to :back
  end
  
  def already_voted?(participant)
    token = "123123321"
    token = current_user._id.to_s if current_user
    cookies["liked_"+participant._id.to_s+"_"+token]
  end  
  
  def comment
    c = Comment.create(:text=>params[:text], :commentable=>@participant, :user=>current_user)
    redirect_to :back
  end  
  
  # GET /participants
  # GET /participants.json
  def index
    #@participants = Participant.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @participants }
    end
  end

  # GET /participants/1
  # GET /participants/1.json
  def show
    @competition = @participant.competition
    @already_voted = already_voted? @participant
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @participant }
    end
  end

  # GET /participants/new
  # GET /participants/new.json
  def new
    #@participant = Participant.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @participant }
    end
  end

  # GET /participants/1/edit
  def edit
    #@participant = Participant.find(params[:id])
    @competition = @participant.competition
  end

  # POST /participants
  # POST /participants.json
  def create
    @competition = Competition.find(params[:participant][:competition_id])
    
    # cannot register more than once to one competition
    if @competition.participating? current_user
      return
    end

    @participant = Participant.new(params[:participant])
    @participant.user_id = current_user._id
    @participant.like = 0

        # TODO: check @participant.competition_id

    respond_to do |format|
      if @participant.save
        format.html { redirect_to @participant, notice: 'Participant was successfully created.' }
        format.json { render json: @participant, status: :created, location: @participant }
      else
        format.html { render action: "new" }
        format.json { render json: @participant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /participants/1
  # PUT /participants/1.json
  def update
    #@participant = Participant.find(params[:id])

    respond_to do |format|
      if @participant.update_attributes(params[:participant])
        format.html { redirect_to @participant, notice: 'Participant was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @participant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /participants/1
  # DELETE /participants/1.json
  def destroy
    #@participant = Participant.find(params[:id])
    competition = @participant.competition
    @participant.destroy

    respond_to do |format|
      format.html { redirect_to competition }
      format.json { head :no_content }
    end
  end
end
