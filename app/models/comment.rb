class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :text, type: String
  field :commentable_type, type: String
 
  belongs_to :commentable, :polymorphic => true
  belongs_to :user
  has_many :comments, :as => :commentable

end