class Participant
	include Mongoid::Document
	include Mongoid::Paperclip

	field :description, type: String
	field :like, type: Integer
	
	has_mongoid_attached_file :picture,
									  :styles => { :medium => "300x300>", :thumb => "75x75>" },
									  :url  => "/assets/participants/:id/:style/:basename.:extension"

	belongs_to :competition
	belongs_to :user
	
	has_many :comments, :as => :commentable

end
