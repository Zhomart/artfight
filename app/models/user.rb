class User
  include Mongoid::Document
  include Mongoid::Paperclip
  field :remember_me, type: Boolean
  field :first_name, type: String
  field :last_name, type: String
  field :aboutme, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  #field :picture, type: String

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :email,              :type => String, :null => false, :default => ""
  field :encrypted_password, :type => String, :null => false, :default => ""

  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name, :aboutme, :picture, :created_at, :updated_at

  has_mongoid_attached_file :picture,
                            :styles => { :medium => ["300x300>", :jpg], :thumb => ["75x75>", :jpg] },
                            :url  => "/assets/avatars/:id/:style/:basename.:extension"
                            
  belongs_to :role
  has_many :participants, dependent: :delete
  has_many :comments

  validates :email, :uniqueness => { :case_sensitive => false }

  def role?(role)
    if self.role.nil?
      return false
    end
    if role.class == Role
      return self.role == role
    end
    if role.class == String || role.class == Symbol
      return self.role == Role.first(conditions: { name: role.to_s })
    end
    
  end

  def fullname
    s = first_name.to_s + ' ' + last_name.to_s
    s = email if s.length < 2
    s
  end
  
  def admin?
    role?('admin')
  end
  
end
