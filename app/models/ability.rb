class Ability
	include CanCan::Ability

	def initialize(user)
		user ||= User.new # guest user

		# give admin permission to all
		#can :manage, :all and return

		can :read, [Competition]
		can [:read, :edit, :update], [User], :_id=>user._id
		can [:read, :create], Participant

		if user.new_record?
			cannot :create, Participant
		elsif user.role? :admin
			can :manage, :all
			can :assign_roles, User
		elsif user.role? :moderator
			can :manage, [Competition]
		else
			# manage products, assets he owns
			%Q{
			can :manage, Product do |product|
				product.try(:owner) == user
			end
			can :manage, Asset do |asset|
				asset.assetable.try(:owner) == user
			end
			}
		end
	end
end