class Competition
	include Mongoid::Document
	include Mongoid::Paperclip
	include Mongoid::MultiParameterAttributes 
	
	field :begin, type: DateTime
   field :end, type: DateTime
   field :title, type: String
   field :description, type: String
  # field :picture, type: String
   field :status, type: Integer
	STATUS_NAMES = {
		0 => 'Not started',
		1 => 'Running',
		2 => 'Suspended',
		3 => 'Finished'
	}

	has_many :participants

	has_mongoid_attached_file :picture,
									  :styles => { :medium => "300x300>", :thumb => "100x100>" },
									  :url  => "/assets/competitions/:id/:style/:basename.:extension"
									  
	def status_name
		STATUS_NAMES[status]
	end
	
	def is_participating(user)
		if user.nil?
			return false
		end
		self.participants.include? Participant.first(conditions: { user_id: user._id } )
	end
	
	def participating?(user)
		if !user.respond_to? :id
			user = User.find(user)
		end
	    participants.where(:user_id => user.id).all.length > 0
	end	
	
	def running?
		now = Time.now
		self.begin <= now and now <= self.end
	end
	
	def participants_desc
		self.participants.desc(:like)
	end
end
