module ApplicationHelper
	def format_time(dt)
		dt.strftime '%d %B, %Y [%H:%M]' unless dt.nil?
	end
	
	def uploaded_image_tag(url, options={})
		output = "<img src='"+url+"'"
		options.each do |key, value|
			output += " "+key.to_s+"='"+value+"' "
		end
		output+=" />"
		sanitize output.html_safe
	end
end
