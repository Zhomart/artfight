class AddRolesAndAdmin < Mongoid::Migration
  USER_EMAIL = 'admin@artfight.kz'
  USER_PASSWORD = '$2a$10$fFDsspxFczWVziLsSfeh/eLQge/wiEnpuxlb8NY6EAhEEOrhcJ2WC' # 111

  def self.up
	radmin = Role.create(:name=>"admin")
	rmoderator = Role.create(:name=>"moderator")

	#radmin = Role.first(conditions: { name: "admin" } )

	#user = User.first(conditions: {email: "admin@artfight.kz" } )

	#user = User.create(:email=>USER_EMAIL, :encrypted_password=>USER_PASSWORD)

	Mongoid.database.collections.find(:name=>"users").first.insert(:email=>USER_EMAIL, :encrypted_password=>USER_PASSWORD)

	user = User.first(conditions: {email: USER_EMAIL } )
	user.created_at = Time.now
	user.updated_at = Time.now
    user.save!

	radmin.users << user
  end

  def self.down
  	Role.all(conditions: { name: "admin" } ).each {|r| r.destroy }
  	Role.all(conditions: { name: "moderator" } ).each {|r| r.destroy }

  	User.all(conditions: { email: "admin@artfight.kz" } ).each {|r| r.destroy }
  end
end